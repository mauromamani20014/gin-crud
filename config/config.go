package config

import (
	"errors"
	"log"

	"github.com/spf13/viper"
)

type Config struct {
  Server ServerConfig
  SQLite SQLite
  Cookie Cookie
}

type ServerConfig struct {
  Port         string
  JWTSecretKey string
}

type SQLite struct {
  SQLiteFileName string
}

type Cookie struct {
  Name     string
  MaxAge   int
  Path     string
  Domain   string
  Secure   bool
  HttpOnly bool
}

// Load config file from given path
func LoadConfig(filename string) (*viper.Viper, error) {
  v := viper.New()

  v.SetConfigName(filename) // nombre del archivo, sin la extencion
  v.AddConfigPath(".") // lugar donde estará el archivo
  v.AutomaticEnv() // para cargar variables de entorno, con esto se sobreescribiran las variables anteriores

  // leemos cada config del ar config.yaml, y retornamos los errores en caso de existir
  if err := v.ReadInConfig(); err != nil {
    if _, ok := err.(viper.ConfigFileNotFoundError); ok {
      return nil, errors.New("Config file not found")
    }  
    return nil, err
  }

  return v, nil
}


// Parse config file
func ParseConfig(v *viper.Viper) (*Config, error) {
  c := &Config{}

  err := v.Unmarshal(c)
  if err != nil {
    log.Printf("Unable to decode into struct, %v", err)
    return nil, err
  }

  return c, nil
}




