package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mauromamani20014/gin-crud/internal/user"
)

func MapUserRoutes(userGroup *gin.RouterGroup, h user.Handlers) {
  userGroup.POST("/new", h.Create)
  userGroup.GET("/all", h.GetAll)
  userGroup.GET("/:id", h.GetById)
  userGroup.PUT("/:id", h.Update)
  userGroup.DELETE("/:id", h.Delete)

  userGroup.POST("/auth/logout", h.Logout)
}
