package http

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"gitlab.com/mauromamani20014/gin-crud/config"
	"gitlab.com/mauromamani20014/gin-crud/internal/entities"
	"gitlab.com/mauromamani20014/gin-crud/internal/user"
	"gitlab.com/mauromamani20014/gin-crud/pkg/httpErrors"
	"gitlab.com/mauromamani20014/gin-crud/pkg/utils"
)

type userHandlers struct {
  cfg     *config.Config
  useCase user.UseCase
}

func NewUserHandlers (cfg *config.Config , userUC user.UseCase) (user.Handlers) {
  return &userHandlers{cfg: cfg, useCase: userUC}
}

// Methods for userHandler

func (h *userHandlers) Logout(c *gin.Context) {
  if _, err := c.Cookie("jwt"); err != nil {
    if errors.Is(err, http.ErrNoCookie) {
      c.JSON(http.StatusUnauthorized, httpErrors.NewUnauthorizedError(err))
      return
    }
    c.JSON(http.StatusInternalServerError, httpErrors.NewInternalServerError(err))
    return
  }

  utils.DeleteJWTCookie(c, h.cfg)

  c.JSON(http.StatusOK, "Cookie deleted!")
}

// Create a user
func (h *userHandlers) Create(c *gin.Context) {
  user := &entities.User{}
  if err := utils.ReadRequest(c, user);err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  createdUser, err := h.useCase.Create(c, user)
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  c.JSON(http.StatusCreated, createdUser)
}

func (h *userHandlers) GetAll(c *gin.Context) {
  users, err := h.useCase.GetAll()
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  c.JSON(http.StatusOK, users)
}

// Get an user by id
func (h *userHandlers) GetById(c *gin.Context) {
  userId, err := strconv.Atoi(c.Param("id"))
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  user, err := h.useCase.GetById(uint(userId))
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  c.JSON(http.StatusOK, user)
}

func (h *userHandlers) Update(c *gin.Context) {
  userId, err := strconv.Atoi(c.Param("id"))
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  attrs := &entities.User{}
  // TODO: ver el orden para evitar que se pueda cambiar el id
  c.Bind(attrs)
  attrs.UserID = uint(userId);

  updatedUser, err := h.useCase.Update(attrs);
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  c.JSON(http.StatusOK, updatedUser)
}

func (h *userHandlers) Delete(c *gin.Context) {
  userId, err := strconv.Atoi(c.Param("id"))
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  if err := h.useCase.Delete(uint(userId)); err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  c.JSON(http.StatusOK, gin.H{
    "message": "User deleted",
  })
}
