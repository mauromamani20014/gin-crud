package repository

import (
	"gitlab.com/mauromamani20014/gin-crud/internal/entities"
	"gitlab.com/mauromamani20014/gin-crud/internal/user"
  "github.com/pkg/errors"
	"gorm.io/gorm"
)

type userRepository struct {
  db *gorm.DB
}

func NewUserRepository(db *gorm.DB) user.Repository {
  return &userRepository{db: db}
}

// Create a new user
func (r *userRepository) Create(user *entities.User) (*entities.User, error) {
  if result := r.db.Create(user); result.Error != nil {
    return nil, errors.Wrap(result.Error, "userRepository.Create")
  }

  return user, nil
}

func (r *userRepository) GetAll() (*[]entities.User, error) {
  users := &[]entities.User{}

  result := r.db.Find(users)

  if result.Error != nil {
    return nil, errors.Wrap(result.Error, "userRepository.GetAll")
  }

  return users, nil
}

func (r *userRepository) GetById(userId uint) (*entities.User, error) {
  foundUser := &entities.User{}

  result := r.db.Where("user_id = ?", userId).First(foundUser)
  if result.Error != nil {
    return nil, errors.Wrap(result.Error, "userRepository.GetById")
  }

  return foundUser, nil
}

// Update existing user
func (r *userRepository) Update(attrs *entities.User, foundUser *entities.User) (*entities.User, error) {
  result := r.db.Model(&foundUser).Updates(attrs)
  if result.Error != nil {
    return nil, result.Error
  }

  return foundUser, nil
}

// Delete existing user
func (r *userRepository) Delete(userId uint) error {
  result := r.db.Delete(&entities.User{}, userId)
  if result.Error != nil {
    return errors.Wrap(result.Error, "userRepository.Delete")
  }

  return nil
}

// Find an user by email
func (r *userRepository) GetByEmail(user *entities.User) (*entities.User, error) {
  foundUser := &entities.User{}

  result := r.db.Where("email = ?", user.Email).First(foundUser)
  if result.Error != nil {
    return nil, errors.Wrap(result.Error, "userRepository.GetByEmail")
  }

  return foundUser, nil
}
