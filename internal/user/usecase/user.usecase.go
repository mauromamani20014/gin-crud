package usecase

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"

	"gitlab.com/mauromamani20014/gin-crud/config"
	"gitlab.com/mauromamani20014/gin-crud/internal/entities"
	"gitlab.com/mauromamani20014/gin-crud/internal/user"
	"gitlab.com/mauromamani20014/gin-crud/pkg/httpErrors"
	"gitlab.com/mauromamani20014/gin-crud/pkg/utils"
)

type userUseCase struct {
  cfg            *config.Config
  userRepository user.Repository
}

func NewUserUseCase(cfg *config.Config, userRepo user.Repository) (user.UseCase) {
  return &userUseCase{cfg: cfg, userRepository: userRepo}
}

// Create a new user
func (u *userUseCase) Create(c *gin.Context, user *entities.User) (*entities.User, error) {
  existsUser, err := u.userRepository.GetByEmail(user)
  if existsUser != nil || err == nil {
    return nil, httpErrors.NewRestError(http.StatusBadRequest, httpErrors.ErrEmailAlreadyExists, nil)
  }

  if err := user.PrepareCreate(); err != nil {
    return nil, httpErrors.NewBadRequestError(errors.Wrap(err, "userUseCase.Create.PrepareCreate"))
  }

  createdUser, err := u.userRepository.Create(user)
  if err != nil {
    return nil, err
  }

  // Create jwt
  token, err := utils.GenerateJWT(createdUser, u.cfg)
  if err != nil {
    return nil, httpErrors.NewInternalServerError(errors.Wrap(err, "userUseCase.Create.GenerateJWT"))
  }

  // Configure cookie
  utils.ConfigureJWTCookie(c, token, u.cfg) 

  return createdUser, nil
}

func (u *userUseCase) GetAll() (*[]entities.User, error) {
  // TODO: pagination, etc
  return u.userRepository.GetAll()
}

// Get an user by id
func (u *userUseCase) GetById(userId uint) (*entities.User, error) {
  user, err := u.userRepository.GetById(userId)
  if err != nil {
    return nil, err
  }

  return user, nil
}

func (u *userUseCase) Update(attrs *entities.User) (*entities.User, error) {
  // TODO: hash password
  foundUser, err := u.GetById(attrs.UserID)
  if err != nil {
    return nil, err
  }

  updatedUser, err := u.userRepository.Update(attrs, foundUser)
  if err != nil {
    return nil, err
  }

  return updatedUser, nil
}

func (u *userUseCase) Delete(userId uint) error {
  // verificamos que existe el usuario antes de eliminarlo
  if _, err := u.GetById(userId); err != nil {
    return err
  }

  if err := u.userRepository.Delete(userId); err != nil {
    return err
  }

  return nil
}

