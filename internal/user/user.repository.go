package user

import "gitlab.com/mauromamani20014/gin-crud/internal/entities"

type Repository interface {
  Create(user *entities.User) (*entities.User, error)
  GetAll() (*[]entities.User, error)
  GetById(userId uint) (*entities.User, error)
  GetByEmail(user *entities.User) (*entities.User, error)
  Update(attrs *entities.User, foundUser *entities.User) (*entities.User, error)
  Delete(userId uint) (error)
}
