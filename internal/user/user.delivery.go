package user

import "github.com/gin-gonic/gin"

type Handlers interface {
  Create(c *gin.Context)
  GetAll(c *gin.Context)
  GetById(c *gin.Context)
  Update(c *gin.Context)
  Delete(c *gin.Context)

  Logout(c *gin.Context)
}
