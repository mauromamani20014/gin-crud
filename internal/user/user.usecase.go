package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mauromamani20014/gin-crud/internal/entities"
)

type UseCase interface {
  Create(c *gin.Context, user *entities.User) (*entities.User, error)
  GetAll() (*[]entities.User, error)
  GetById(userId uint) (*entities.User, error)
  Update(user *entities.User) (*entities.User, error)
  Delete(userId uint) (error)
}
