package server

import (
	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"

	userHttp "gitlab.com/mauromamani20014/gin-crud/internal/user/delivery/http"
	userRepo "gitlab.com/mauromamani20014/gin-crud/internal/user/repository"
	userUC "gitlab.com/mauromamani20014/gin-crud/internal/user/usecase"
)

func (s *Server) MapHandlers(r *gin.Engine) error {
  // Global Middleware
  r.Use(cors.AllowAll())

  v1 := r.Group("/api/v1")

  // Init groups
  userGroup := v1.Group("/user")

  // Init repositories
  userRepository := userRepo.NewUserRepository(s.db)

  // Init useCases
  userUseCase := userUC.NewUserUseCase(s.cfg, userRepository)

  // Init handlers
  userHandler := userHttp.NewUserHandlers(s.cfg, userUseCase)

  // Init Routes
  userHttp.MapUserRoutes(userGroup, userHandler)

  return nil
}
