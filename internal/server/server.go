package server

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"gitlab.com/mauromamani20014/gin-crud/config"
)

type Server struct {
  gin *gin.Engine
  db  *gorm.DB
  cfg *config.Config
}

func NewServer(db *gorm.DB, cfg *config.Config) *Server {
  return &Server{gin: gin.Default(), db: db, cfg: cfg}
}

func (s *Server) Run() error {
  if err := s.MapHandlers(s.gin); err != nil {
    return err
  }

  // Running Server
  if err := s.gin.Run(s.cfg.Server.Port); err != nil {
    return err
  }

  return nil
}
