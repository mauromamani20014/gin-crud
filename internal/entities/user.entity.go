package entities

import (
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
  UserID    uint      `json:"user_id"    gorm:"primaryKey"`
  FirstName string    `json:"first_name" validate:"required"`
  LastName  string    `json:"last_name"  validate:"required"`
  Email     string    `json:"email"      validate:"required,email" gorm:"unique"`
  Password  string    `json:"password"   validate:"required,passwd"`
  CreatedAt time.Time `json:"created_at"`
  UpdatedAt time.Time `json:"updated_at"`
}

// Hash user password with bcrypt
func (u *User) HashPassword() error {
  hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
  if err != nil {
    return  err
  }
  u.Password = string(hashedPassword)

  return nil
}

// Compare user password and payload
func (u *User) ComparePassword(password string) error {
  if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password)); err != nil {
    return err
  }

  return nil
}

// Prepare user for create
func (u *User) PrepareCreate() error {
  u.Email = strings.ToLower(strings.TrimSpace(u.Email))
  u.Password = strings.TrimSpace(u.Password)

  if err := u.HashPassword(); err != nil {
    return err
  }

  return nil
}
