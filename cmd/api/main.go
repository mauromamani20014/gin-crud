package main

import (
	"log"
	"os"

	"gitlab.com/mauromamani20014/gin-crud/config"
	"gitlab.com/mauromamani20014/gin-crud/internal/server"
	"gitlab.com/mauromamani20014/gin-crud/pkg/db/sqlite"
	"gitlab.com/mauromamani20014/gin-crud/pkg/utils"
)

// TODO: Add loggers
func main() {
  // Load config
  configPath := utils.GetConfigPath(os.Getenv("config"))

  configFile, err := config.LoadConfig(configPath)
  if err != nil {
    log.Fatalf("LoadConfig: %v", err)
  }

  cfg, err := config.ParseConfig(configFile)
  if err != nil {
    log.Fatalf("ParseConfig: %v", err)
  }

  // Connection to db
  db, err := sqlite.NewSqliteDB(cfg)
  if err != nil {
    panic(err)
  } else {
    log.Println("Database connected!")
  }

  // Migrations
  sqlite.AutoMigrate(db)

  // New Server
  s := server.NewServer(db, cfg)

  if err := s.Run(); err != nil {
    panic(err)
  }
}

