package sqlite

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gitlab.com/mauromamani20014/gin-crud/config"
	"gitlab.com/mauromamani20014/gin-crud/internal/entities"
)

// NewDatabase: return new sqlite instance
func NewSqliteDB(c *config.Config) (*gorm.DB, error) {
  db, err := gorm.Open(sqlite.Open(c.SQLite.SQLiteFileName), &gorm.Config{})
  if err != nil {
    return nil, err
  }

  return db, nil
}

func AutoMigrate(db *gorm.DB) {
  db.AutoMigrate(
    &entities.User{},
  )
}
