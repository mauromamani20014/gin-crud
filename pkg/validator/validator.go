package validator

import (
	"log"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
	"gitlab.com/mauromamani20014/gin-crud/pkg/converter"
)

var validate *validator.Validate
var uni      *ut.UniversalTranslator
var trans    ut.Translator

func init() {
  en := en.New()
  uni = ut.New(en, en)

  trans, _ = uni.GetTranslator("en")

  validate = validator.New()

  if err := en_translations.RegisterDefaultTranslations(validate, trans); err != nil {
    log.Fatal(err)
  }
}

func ValidateStruct(s interface{}) error {
  loadValidatorConfig()

  return validate.Struct(s)
}

// MapErrors: map errors 
func MapErrors(err error) (map[string]interface{}) {
  errors := make(map[string]interface{})

  for _, err := range err.(validator.ValidationErrors) {
    errors[converter.ToSnakeCase(err.Field())] = err.Translate(trans)
  }

  return errors
}

