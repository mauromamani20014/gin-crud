package utils

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mauromamani20014/gin-crud/config"
)

func ConfigureJWTCookie(c *gin.Context, token string, cfg *config.Config) {
  c.SetCookie(
    cfg.Cookie.Name,
    token,
    cfg.Cookie.MaxAge,
    cfg.Cookie.Path,
    cfg.Cookie.Domain,
    cfg.Cookie.Secure,
    cfg.Cookie.HttpOnly,
  )
}

func DeleteJWTCookie(c *gin.Context, cfg *config.Config) {
  c.SetCookie(
    cfg.Cookie.Name,
    "",
    -1,
    cfg.Cookie.Path,
    cfg.Cookie.Domain,
    cfg.Cookie.Secure,
    cfg.Cookie.HttpOnly,
  )
}
