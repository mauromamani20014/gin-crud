package utils

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/mauromamani20014/gin-crud/pkg/validator"
)

// Get config path for local or docker
func GetConfigPath(configPath string) string {
	if configPath == "docker" {
		return "./config/config-docker"
	}
	return "./config/config-local"
}

func ReadRequest(c *gin.Context, request interface{}) error {
  if err := c.Bind(request); err != nil {
    return err
  }
  return validator.ValidateStruct(request)
}
