package utils

import (
	"errors"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/mauromamani20014/gin-crud/config"
	"gitlab.com/mauromamani20014/gin-crud/internal/entities"
)

const SECRET_KEY = "SECRET_@123"
// JWT Claims
type CustomClaims struct {
  Email string `json:"email"`
  ID    string `json:"id"`
  jwt.StandardClaims
}

func GenerateJWT(user *entities.User, cfg *config.Config) (string, error) {
  // Register the JWT claims, which includes the username and expiry time
  expireTime := time.Now().Add(time.Minute * 60)
  claims := &CustomClaims{
    Email:          user.Email,
    ID:             strconv.FormatUint(uint64(user.UserID), 10),
    StandardClaims: jwt.StandardClaims{
      ExpiresAt: expireTime.Unix(), // Expires 60min
    },
  }

  // Declare the token with the algorithm used for signing, and the claims
  token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

  tokenString, err := token.SignedString([]byte(cfg.Server.JWTSecretKey))
  if err != nil {
    return "", err
  }

  return tokenString, nil
}

func ExtractJWT(cookie string) (map[string]interface{}, error) {
  claims := jwt.MapClaims{}
  token, err := jwt.ParseWithClaims(cookie, claims, func(token *jwt.Token) (interface{}, error) {
    return []byte(SECRET_KEY), nil
  })

  if err != nil {
		if errors.Is(err, jwt.ErrSignatureInvalid) {
			return nil, errors.New("Invalid token signature")
		}
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("Invalid token ")
	}

  return claims, nil
}
